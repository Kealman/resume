package main

import (
	"bufio"
	"code.google.com/p/gcfg"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/sevlyar/go-daemon"
	"github.com/streadway/amqp"
	"log"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"syscall"
	"time"
)

const (
	PROC_NAME = "quotes"
)

var (
	pidFileName    string
	logFileName    string
	configFileName string
)

var (
	vseHrenovo bool
)

type QuoteRow struct {
	Ask   int64
	Bid   int64
	Price int64
}

type QuoteMessage struct {
	Date   string           `json:"date"`
	Quotes map[string]int64 `json:"quotes"`
}

type QuoteHistoryMessage struct {
	ID     string         `json:"id"`
	Result []QuoteMessage `json:"result"`
}

func Work() {
	// Make config storage
	config := struct {
		Rabbit struct {
			Host     string
			Port     int
			Username string
			Password string
			Suffix   string
		}
		Pgsql struct {
			Host     string
			Port     int
			Username string
			Password string
			Database string
			Schema   string
		}
		Unidde struct {
			Host     string
			Port     int
			Username string
			Password string
		}
	}{}

	// Read configuration
	env := os.Environ()
	if err := gcfg.ReadFileInto(&config, env[0]); err != nil {
		log.Fatalln(err)
	}

	fmt.Println("STARTED\n=======")

	quotes := make(map[string]int64)
	quoteRows := make(map[string]QuoteRow)

	// connect to PostgreSQL
	db, err := sql.Open("postgres", "host="+config.Pgsql.Host+" port="+strconv.Itoa(config.Pgsql.Port)+" user="+config.Pgsql.Username+" password="+config.Pgsql.Password+" dbname="+config.Pgsql.Database+" sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}
	defer db.Close()

	// connect to RabbitMQ
	rabbit, err := amqp.Dial("amqp://" + config.Rabbit.Username + ":" + config.Rabbit.Password + "@" + config.Rabbit.Host + ":" + strconv.Itoa(config.Rabbit.Port) + "/%2F")
	if err != nil {
		log.Fatalln(err)
	}
	defer rabbit.Close()

	// create RabbitMQ channel
	channel, err := rabbit.Channel()
	if err != nil {
		log.Fatal(err)
	}

	// declare exchanges
	if err = channel.ExchangeDeclare("option.quotes", "fanout", true, false, false, false, nil); err != nil {
		log.Fatal(err)
	}

	// declare internal channels
	tickChan := time.NewTicker(time.Second * 1).C
	dropChan := time.NewTicker(time.Minute * 30).C // `deleter channel`

	// prepare SQL statements
	newQuoteStms, err := db.Prepare(`INSERT INTO quotes.quotes_history(symbol, ts, ask, bid, price) VALUES ($1, $2, $3, $4, $5);`)
	if err != nil {
		log.Fatal(err)
	}

	dropQuoteStms, err := db.Prepare(`DELETE FROM quotes.quotes_seconds WHERE ts < now() - interval '30 minute'`)
	if err != nil {
		log.Fatal(err)
	}

	vseHrenovo = true

	// quotes processing
	go func() {
		quotePreg := regexp.MustCompile(`(.+)\s(\d+(\.\d+)*)\s(\d+(\.\d+)*)`)

		for {
			// connect to quote source socket
			sock, err := net.Dial("tcp", config.Unidde.Host+":"+strconv.Itoa(config.Unidde.Port))
			if err != nil {
				continue
			}

			vseHrenovo = false

			sock.Write([]byte(config.Unidde.Username + "\n"))
			sock.Write([]byte(config.Unidde.Password + "\n"))

			buffer := bufio.NewReader(sock)

			for {
				str, err := buffer.ReadString('\n')

				if err != nil {
					sock.Close()

					vseHrenovo = true

					break
				}

				// quote string looks normal
				if quotePreg.MatchString(str) {
					parts := quotePreg.FindAllStringSubmatch(str, -1)

					symbol := parts[0][1]
					ask, _ := strconv.ParseFloat(parts[0][2], 64)
					bid, _ := strconv.ParseFloat(parts[0][4], 64)

					askInt := int64(ask * 1000000)
					bidInt := int64(bid * 1000000)

					price := int64((askInt+bidInt)/2) + int64(rand.Int31n(9))
					quotes[symbol] = price

					quoteRows[symbol] = QuoteRow{
						Ask:   askInt,
						Bid:   bidInt,
						Price: price,
					}
				}
			}

		}
	}()

	// main loop
	for {
		select {
		// every second
		case <-tickChan:
			// process quotes
			go func() {
				tm := time.Now().Format(time.RFC3339)

				go func() {
					for symbol, row := range quoteRows {
						_, err := newQuoteStms.Exec(symbol, tm, row.Ask, row.Bid, row.Price)
						if err != nil {
							log.Fatal(err)
						}
					}
				}()

				if vseHrenovo == false {
					quoteMsg := &QuoteMessage{
						Date:   tm,
						Quotes: quotes,
					}

					jsonQuote, _ := json.Marshal(quoteMsg)
					channel.Publish("option.quotes", "", false, false, amqp.Publishing{
						Expiration:  "10000",
						Body:        jsonQuote,
						ContentType: "application/json",
					})
				}
			}()

		// every 30 minutes do cleaning
		case <-dropChan:
			go func() {
				_, err := dropQuoteStms.Exec()
				if err != nil {
					log.Fatal(err)
				}
			}()
		}
	}
}

func main() {
	// If not daemonize yet
	if !daemon.WasReborn() {
		flag.StringVar(&pidFileName, "p", "", "pid filename")
		flag.StringVar(&logFileName, "l", "", "log filename")
		flag.StringVar(&configFileName, "c", "", "filename of configuration file")
		flag.Parse()

		// Check parameters
		if pidFileName == "" || logFileName == "" {
			fmt.Println("Usage: ./" + PROC_NAME + " -p pid_file -l log_file -c config_file")
			return
		}
	}

	// Make daemon context
	context := &daemon.Context{
		Umask:       027,
		WorkDir:     "./",
		PidFilePerm: 0644,
		LogFilePerm: 0640,
		PidFileName: pidFileName,
		LogFileName: logFileName,
		Args:        []string{PROC_NAME},
		Env:         []string{configFileName},
	}

	// Daemonize
	imp, err := context.Reborn()
	if err != nil {
		log.Fatalln(err)
	}

	if imp != nil {
		return
	}

	defer context.Release()

	// Run worker
	go Work()

	log.Println("--------------")
	log.Println("started")

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM)

	// Terminate
	<-signalChan

	log.Println("terminated\n")
}
