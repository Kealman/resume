
var path = require('path'),
  fs = require('fs'),
  cjson = require('cjson'),
  _ = require('underscore'),
  glob = require('glob');

var getConfigFiles = function (pathConfig, routerPath, env) {
  var configFileArray = [];
  var configFileArrayEnv = [];
  var configFileArrayRoute = [];


  if (!pathConfig) {
    throw new Error('Path in required');
  }
  if (!fs.existsSync(pathConfig)) {
    throw Error('Config path incorrect');
  }

  glob.sync('./**/*.json',{root: path.join(pathConfig, 'default'), cwd:  path.join(pathConfig, 'default')}).forEach(function(el){
    configFileArray.push({filePath: path.join(pathConfig, 'default', el), name: el});
  });

  if (!_.isUndefined(env)) {
    glob.sync('./**/*.json', {
      root: path.join(pathConfig, env),
      cwd: path.join(pathConfig, env)
    }).forEach(function (el) {
      configFileArrayEnv.push({filePath: path.join(pathConfig, env, el), name: el});
    });
  }

  if (!_.isUndefined(routerPath)) {
    glob.sync('./**/*.json', {
      root: path.join(pathConfig, routerPath),
      cwd: path.join(pathConfig, routerPath)
    }).forEach(function (el) {
      configFileArrayRoute.push({filePath: path.join(pathConfig, routerPath, el), name: el});
    });
  }

  return {
    configFileArray: configFileArray,
    configFileArrayEnv: configFileArrayEnv,
    configFileArrayRoute: configFileArrayRoute
  };
};

/**
 * Safe set value in object
 *
 * Example:
 * var obj = {};
 * var objsame = safeSet(obj, 'key1.key2.key3', 'Hello')
 * assert(obj, {key1: {key2: {key3: 'Hello'}}})
 * assert(obj, objsame)
 *
 * @param obj Original object
 * @param key Key to set, example 'test.sub.key'
 * @param value Some any value of key
 */
var safeSet = function(obj, key, value) {
  var parts = key.split('.');
  if (parts.length === 0){
    throw new TypeError('Key must be not empty');
  }
  for(var i = 0; i < parts.length-1; i++){
    var part = parts[i];
    if (!obj.hasOwnProperty(part)){
      obj[part] = {};
    }
    obj = obj[part];
  }
  obj[parts[parts.length-1]] = value;
  return obj;
};

/**
 * Save get value by key from input object
 *
 * Example:
 * var obj = {key1: {key2: {key3: 'Hello'}}}
 * var value = safeGet(obj, 'key1.key2.key3', null)
 * assert(value, 'Hello')
 * var value = safeGet(obj, 'key1.key2.key4', null)
 * assert(value, null)
 *
 * @param obj Original object
 * @param key Key to get, example 'test.sub.key'
 * @param defaultValue Default value of key not found
 */
var safeGet = function(obj, key, defaultValue) {
  var parts = key.split('.');
  if (parts.length === 0){
    throw new TypeError('Key must be not empty');
  }
  for(var i = 0; i < parts.length-1; i++){
    var part = parts[i];
    if (!obj.hasOwnProperty(part) || !_.isObject(obj[part])){
      return defaultValue;
    }
    obj = obj[part];
  }
  if (!obj.hasOwnProperty(parts[parts.length-1])){
    return defaultValue;
  } else {
    return obj[parts[parts.length-1]];
  }
};

var mapConfigFiles = function(filesArray) {
  var getNameParts = function(name){
    var nameParts = name.split(path.sep).filter(function(el){ return el !== '' && el !== '.'; });
    var last = nameParts[nameParts.length-1];
    last = path.basename(last, path.extname(last));
    nameParts[nameParts.length-1] = last;
    return nameParts;
  };
  var prefixes = {};
  filesArray.forEach(function(el){
    var nameParts = getNameParts(el.name);
    var files = safeGet(prefixes, nameParts.join('.'), undefined);
    if (_.isUndefined(files)) {
      files = [];
      safeSet(prefixes, nameParts.join('.'), files);
    }
    files.push(el.filePath);
  });
  filesArray.forEach(function(el){
    var nameParts = getNameParts(el.name);
    var files = safeGet(prefixes, nameParts.join('.'), []);
    var obj = cjson.load(files);
    nameParts.pop();
    _.each(obj, function(value, key){
      safeSet(prefixes, nameParts.concat([key]).join('.'), value);
    });

  });
  return prefixes;
};

var providerConfig = function (pathConfig, routerPath, env) {
  env = env || process.env.NODE_ENV || 'development';
  var configFiles = getConfigFiles(pathConfig, routerPath, env);

  var defaultConfig = mapConfigFiles(configFiles.configFileArray),
    envConfig = mapConfigFiles(configFiles.configFileArrayEnv),
    routeConfig = mapConfigFiles(configFiles.configFileArrayRoute);

  var resultObjectConfig = cjson.extend(true, defaultConfig, envConfig);

  if (!_.isUndefined(resultObjectConfig.env)) {
    throw new Error('The reserved config name \'env\'');
  }
  if (!_.isUndefined(resultObjectConfig.router)) {
    throw new Error('The reserved config name \'router\'');
  }

  resultObjectConfig.router = routeConfig;
  resultObjectConfig.env = env;

  this.stores = resultObjectConfig;
};

providerConfig.prototype.get = function (name) {

  if (_.isUndefined(name) || !_.isString(name)) {
    throw new Error('Config name is required and must be String');
  }

  var keys = name.split(':');

  function exec(obj, arr) {
    if (arr.length > 0 && !obj[arr[0]]) {
      throw new Error('Wrong config name: ' + name);
    }
    return arr.length !== 0 ? exec(obj[arr[0]], arr.slice(1)) : (typeof obj === 'object' ? _.clone(obj) : obj);
  }

  return  exec(this.stores, keys);

};


module.exports = providerConfig;
